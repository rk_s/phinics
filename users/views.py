from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm
# Create your views here.
def register(request):
	if (request.method == 'POST'):
		print('rakesh1')
		form = UserRegisterForm(request.POST)
		print(form.is_valid())
		username = form.cleaned_data.get('username')
		if username.is_valid():
			#print(form)
			form.save()
			username = form.cleaned_data.get('username')
			messages.success(request, f'Account created for {username}!')
			return redirect('blog-home')
		else:
			print(form)
			return redirect('register')

	else:
		form = UserRegisterForm()
		return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
	return render(request, 'users/profile.html')
